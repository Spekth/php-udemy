<?php 
  $meses = [
    'Enero','Febrero','Marzo',
    'Abril','Mayo','Junio','Julio',
    'Agosto','Septiembre','Octubre',
    'Noviembre','Diciembre'
  ];
  $numeros = [1,22,10,9,5,70,100,20];
  sort($numeros);
  // rsort($meses);
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Meses del año</title>
</head>
<body>
  <h1>Meses del Año</h1>
  <ul>
    <?php 
      foreach($numeros as $numero){
        echo '<li>' . $numero . '</li>'; 
      };
    ?>
  </ul>
</body>
</html>