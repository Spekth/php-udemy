<?php
/**
 * var_dump($variable)
 * Nos permite conocer el valor que tenemos en una variable y darnos algunas
 * otras propiedades que tienen las variables; 
 */
$texto = 'Carlos';
$numero = 10;
$numero2 = '5';
$arreglo = array('Carlos','Cesar','Alejandro');
$arreglo_asociativo = array('nombre' => 'Neider', 'edad' => 20);
$booleano = false;

echo '<pre>';
var_dump($booleano);
echo '</pre';
?>