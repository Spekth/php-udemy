<?php 
  /**
   * hmtlspeicalchars : Nos permite convertir caracteres especiales a entidades HTML
   * trim() : Nos permite eliminar los espacios en blanco que se encuentran al principio y al final de una cadena de caracteres
   * strlen : Nos permite conocer la cantidad de caracteres de una variable 
   * substring : Nos permite retornar un pedazo de una cadena de texto.
   * strtoupper: Convierte una cadena a Mayúsculas.
   * strtolower: Convierte una cadena a Minusculas.
   * strpost: Nos permite conocer donde se encuentra una letra;
  */

  // $texto = '< > & "" ';
  // echo htmlspecialchars($texto);
  // trim($texto);
  // echo strlen($texto);
  
  $texto = 'Hola Neider';
  echo strpos($texto,'H');
?>