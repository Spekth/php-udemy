<?php
  /**
   * stract(): Nos permite extraer los elementos de un arreglo asociativo como si fueran variables;
   * pop(): Nos permite extraer el último elemento de un array y eliminarlo. 
   * join(): Nos permite separar los elementos de un array.
   * count(): Nos permite conocer cuantos elementos tenemos en el arreglo
   * sort(): Nos permite mostrar nuestro arreglo en orden ascendente
   * rsort(): Nos permite mostrar nuestro arreglo en forma descente.
   * array_reverse: Nos permite revertir el orden de los elementos de un array
  */

  // $amigo = array('telefono' => 3234561278,'edad' => 20, 'pais' => 'Mexico');
  // extract($amigo);

  // echo $telefono;

  $semana = array(
    'Lunes','Martes','Miercoles','Jueves','Viernes','Sábado','Domingo'
  );

  // $ultimo_dia = array_pop($semana);
  // // foreach($semana as $dia){
  // //   echo $dia . '<br/>';
  // // }
  // echo $ultimo_dia;

  // sort($semana);
  // echo join(', ', $semana);

 $semana_reverse = array_reverse($semana);
 echo join(', ', $semana_reverse);
?>