<?php 
  /**
   * TIPOS DE DATOS
   * String: Cadena de texto
   * Integer: Números enteros
   * Double: Números con decimales
   * Boolean: Verdadero o Falso (true / false)
   * 
   * Array: Arreglo
   * Object: Objeto
   * Class: Clase
   * Null: Cuando a una variable no se le ha asignado ningun valor
   */
  $nombre = 'Neider López';
  $numero = 7;
  $numero_decimal = 7.7;
  $verdadero = true;
  $edad;
  # Las comillas dobles "" permiten dentro llamar variables.

  # Para conocer el tipo de dato de una variable usamos la función 
  # gettype(variable);
  echo gettype($numero);
?>