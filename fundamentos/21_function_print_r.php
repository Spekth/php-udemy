<?php
/**
 * print_r($variable)
 * Nos permite conocer el valor de una variable
 */
$texto = 'Carlos';
$numero = 10;
$numero2 = '5';
$arreglo = array('Carlos','Cesar','Alejandro');
$arreglo_asociativo = array('nombre' => 'Neider', 'edad' => 20);
$booleano = false;

// echo '<pre>';
print_r($booleano);
// echo '</pre';
?>