<?php 
  /* 
    NOTA: Para trabajar con variables que estan fuera de una función hay que pasarlas 
    como parametro
  */
  
  $numero = 7;
  function mostrarNumero($numero){
    echo $numero;
  }

  mostrarNumero($numero);
?>