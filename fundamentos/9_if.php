<?php 

/**
 * OPERADORES DE COMPARACIÓN
 * != Diferente
 * ! Negación 
 * 
 * OPERADORES LOGICOS
 * && 
 * || OR
 * xor - Evalua que se cumpla una y solo una condición
*/
  $edad = 18;
  $nombre = 'Neider';
  if($edad >= 18 || $nombre == 'Neider'){
    echo '<h1>Bienvenido</h1>';
  }

  /* if($edad < 18 or $nombre != 'Neider'){
    echo '<h1>Eres menor de edad y/0 no te llamas Neider</h1>';
  } */
?>