<?php 
  class Persona {
    public $nombre;
    public $edad;
    public $profesion;

    function __construct($nombre,$edad,$profesion){
      $this->nombre = $nombre;
      $this->edad = $edad;
      $this->profesion = $profesion;
    }
    public function mostrarInformacion(){
      echo $this->nombre . ' tienes ' . $this->edad . ' anios de edad ' . ' y eres ' . $this->profesion;
    }
  }
  
  $neider = new Persona('Neider',25,'Developer JavaScript'); 
  $neider->mostrarInformacion();
?>