<?php
  class Persona {
    public $nombre;
    public $edad;
    public $pais;

    function __construct($nombre,$edad,$pais){
      // echo 'Hola Mundo '.$nombre;
      $this->nombre = $nombre;
      $this->edad = $edad;
      $this->pais = $pais;
    }
    
    public function mostrarInformacion(){
      echo $this->nombre . ' tiene ' . $this->edad . ' anios de edad y es de ' . $this->pais;
    }
  }

  $carlos = new Persona('Carlos Arturo',23,'Mexico');
  // $carlos->nombre = 'Carlos Arturo';
  // $carlos->edad = 23;
  // $carlos->pais = 'Mexico';
  $carlos->mostrarInformacion();

  echo '<br />';
  
  $alejandro = new Persona('Alejandro',30,'Espana');
  $alejandro->mostrarInformacion();
  
?>