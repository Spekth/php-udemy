<?php 
  /* Ejemplo 1 - COMO LO HARIAMOS SIN LA POO 
  $nombre = 'Carlos';
  $edad = 23;
  $pais = 'Mexico';

  $nombre2 = 'Alejandro';
  $edad2 = 30;
  $pais2 = 'Espana';

  echo $nombre; */

  // UNA CLASE ES UNA PLANTILLA PARA UN OBJETO

  class Persona {
    public $nombre;
    public $edad;
    public $pais;

    public function mostrarInformacion(){
      echo 'Hola Mundo';
    }
  }


  $carlos = new Persona;
  $carlos-> nombre = 'Carlos Arturo';
  $carlos->edad = 23;
  $carlos->pais =  'Mexico';

  // $carlos->mostrarInformacion();

  // echo $carlos->nombre . ' tiene ' . $carlos->edad . ' anios de edad';

  $alejandro = new Persona;
  $alejandro->nombre = 'Alenjandro';
  $alejandro->edad = 30;
  $alejandro->pais = 'Espana';
?>